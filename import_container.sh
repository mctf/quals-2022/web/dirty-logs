#!/bin/bash

docker image rm ctf1:latest
docker import ctf1_cont
ctf1_image_name=$(docker images | awk '{print $3}' | awk 'NR==2')
docker image tag $ctf1_image_name ctf1:latest

