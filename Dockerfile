FROM ctf1:latest
ARG CTF1_FLAG
CMD ["/bin/bash"]
ADD config_flag.py /root/
RUN python3 /root/config_flag.py $CTF1_FLAG;rm /root/config_flag.py
WORKDIR /home/server
